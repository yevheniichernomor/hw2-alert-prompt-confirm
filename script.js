/* Відповіді:

1.  У JS існує 7 типів данних:
    - Number;
    - String;
    - Bolean;
    - Undefined;
    - Null;
    - Symbol;
    - Object.

2.  == - це нестроге порівняняя, яке порівнює тільки значення змінних.
    === - це строге порівняння, яке порівнює значення змінних та їх типи.
    
3.  Оператор - це вунтрішня функція JS. 
*/

let name = prompt('What is your name?');
while (name == null || name == "") {
    if (name == null) {
        name = prompt('Incorrect, try again.', "");
    }
    else {
        name = prompt('Incorrect, try again.', name)
    }
}

let age = prompt('How old are you?');
while (age == "" || isNaN(age)) {
    age = prompt('Incorrect, try again.', age) 
}
if(age < 18) {
    alert('You are not allowed to visit this website.');
}
else if( age >= 18 && age <= 22) {
    let res = confirm('Are you sure you want to continue?');
    if(res) {
        alert('Welcome, ' + name);
    } 
    else { 
        alert('You are not allowed to visit this website.');
    }
}
else {
    alert('Welcome, ' + name);
}